/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
browser.pageAction.onClicked.addListener(async function (tab, onClickData) {
	const {openerId} = await browser.storage.local.get({openerId: 0});
	await _go(tab, _openers[openerId].func);
});

async function _go(tab, openerFunc) {
	if (m = tab.url.match(/portal\/view\/(\d+)/)) {
		submissionId = m[1];
		await openerFunc(submissionId, tab);
	}
}

async function _goAlways(tab, openerId) {
	await browser.storage.local.set({openerId: openerId});
	await _go(tab, _openers[openerId].func);
}

_openers = [
	{
		name: "NGMVS",
		func: (submissionId, tab) => _openTab(_buildElementURL(submissionId, "app.ngmvs.one"), tab),
	},
	{
		name: "Element Web",
		func: (submissionId, tab) => _openTab(_buildElementURL(submissionId, "app.element.io"), tab),
	},
	{
		name: "matrix.to",
		func: (submissionId, tab) => _openTab(_buildURL(submissionId, "https://matrix.to/#/#"), tab),
	},
	{
		name: "Browser default",
		// TODO Consider adding "action=join" query parameter
		func: (submissionId, tab) => _navTo(_buildURL(submissionId, "matrix:r/", "?via=ngmvs.one"), tab),
	},
];

function _buildElementURL(submissionId, elementHost) {
	return _buildURL(submissionId, `https://${elementHost}/#/room/#`);
}

function _buildURL(submissionId, prefix, suffix) {
	return `${prefix || ""}portal_view_${submissionId}:ngmvs.one${suffix || ""}`
}

async function _openTab(url, tab) {
	await browser.tabs.create({
		url: url,
		index: tab.index + 1,
		windowId: tab.windowId,
		cookieStoreId: tab.cookieStoreId,
	});
}

async function _navTo(url, tab) {
	await browser.tabs.update(tab.id, {url: url});
}


const _menuContexts = ["page_action"];
const _menuIdJustOnce = "_mJustOnce";
const _menuIdRemember = "_mRemember";
const _menuItems = [
	{
		contexts: _menuContexts,
		title: "This time, view in...",
		id: _menuIdJustOnce,
	},
	{
		contexts: _menuContexts,
		title: "Remember && view in...",
		id: _menuIdRemember,
	},
	..._openers.map(({name, func}) => ({
		contexts: _menuContexts,
		title: name,
		parentId: _menuIdJustOnce,
		onclick: (info, tab) => _go(tab, func),
	})),
	..._openers.map((opener, openerId) => ({
		contexts: _menuContexts,
		title: opener.name,
		parentId: _menuIdRemember,
		onclick: (info, tab) => _goAlways(tab, openerId),
	})),
].forEach(createProperties => browser.menus.create(createProperties));
